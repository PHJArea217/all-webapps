#!/bin/sh
set -eu

mkdir -p tmp public
cat > public/index.html <<\EOF
<!DOCTYPE html>
<html>
<head>
<title>www.apps.pjin.org</title>
</head>
<body>
The following web apps are hosted here on www.apps.pjin.org, the hosting domain for Peter Jin's static web apps:
<ul>
<!-- ' -->
EOF
while read -r DIR URL WIKI NAME; do
    mkdir -p public/"$DIR"
    wget -O - > tmp/tmparch.zip "$URL"
    bsdtar -xC public/"$DIR" --strip-components 1 -f tmp/tmparch.zip
    printf >> public/index.html '<li><a href="/%s">%s</a> (more info <a href="https://www.peterjin.org/wiki/%s">here</a>)</li>\n' "$DIR" "$NAME" "$WIKI"
done < list.txt
cat >> public/index.html <<\EOF
</ul>
<p>For other web apps, see <a href="https://www.peterjin.org/wiki/List_of_web_apps">a complete listing of web apps available on peterjin.org</a>.
EOF
rm -f tmp/tmparch.zip
