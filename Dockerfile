FROM ubuntu:20.04
RUN apt-get install nginx libarchive-tools
CMD nginx -g 'daemon off' -p /etc/nginx -c /etc/nginx/local/nginx.conf
